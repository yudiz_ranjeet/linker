const { gql } = require('apollo-server')
module.exports = gql`
scalar Date
scalar Link
scalar Image
type User {
    _id: String,
    name: String,
    email:String,
    mobile: String,
    profile: Image,
    createdAt: Date,
    sToken: String
}
type userDetails {
    _id: String,
    name: String,
}
input UserInput{
    name: String!,
    email:String,
    mobile: String!,
    password: String!,
}
type question {
    _id: String,
    question: String,
    answer: [answer],
    AskedBy: userDetails,
    createdAt: Date
}

input QuestionInput {
    question: String!
}

type answer {
    _id: String,
    question: String,
    description: String,
    answer : Link,
    AnswerBy: userDetails,
    createdAt: Date,
    likes: [String]
}

input LoginInput {
    email: String!,
    password: String!
}

input AnswerInput {
    questionId: String!,
    answer : Link!,
    description: String!
}

type Query{
    question(ID:ID!): question!
    answer(ID:ID!): answer!
    user(ID:ID!): User!
    getAllAnswer(limit: Int): [answer]
    getAllQuestion(limit :Int): [question]
    getUser(limit: Int): [User]!
    findQuestion(questionInput: QuestionInput):[question],
    likeWiseQuestion(limit: Int): [answer]
}

type Mutation{
    createQuestion(questionInput: QuestionInput): question!
    deleteQuestion(ID: ID!): String
    editQuestion(ID:ID!, questionInput: QuestionInput): question!
    
    createAnswer(answerInput: AnswerInput): answer!
    deleteAnswer(ID: ID!): String
    editAnswer(ID:ID!, answerInput: AnswerInput): answer!
    likeAnswer(ID:ID!): Boolean

    createUser(userInput: UserInput): String
    editUser(userInput:UserInput): String
    loginUser(loginInput: LoginInput): User!
    logOutUser(ID: ID): Boolean
    deleteUser(ID: ID!): Boolean
}
`
