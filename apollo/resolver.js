const User = require('../models/user')
const { ApolloError, UserInputError } = require('apollo-server-express')
const Questions = require('../models/question')
const Answer = require('../models/answer')
const path = require('path')
const jwt = require('jsonwebtoken')
const URL = require('url').URL
const imageExtensions = require('image-extensions')
const graphql = require('graphql')
const {
  GraphQLScalarType,
  GraphQLError
} = graphql
const { validateMobile, validatePassword, validateEmail, isValidObjectId } = require('../helper/helper')

const isImage = value => {
  if (typeof value !== 'string') throw new GraphQLError('Value passed is not a string')
  const extension = path.extname(value).slice(1)
  if (imageExtensions.includes(extension)) return value
  throw new GraphQLError('Value passed is not an Image')
}

const Image = new GraphQLScalarType({
  name: 'Image',
  description: 'An Image Scalar',
  serialize: value => isImage(value)
})

const linkChecker = value => {
  try {
    // eslint-disable-next-line no-new
    new URL(value)
    return value
  } catch (err) {
    throw new UserInputError('Given Answer is not valid Link')
  }
}

const Link = new GraphQLScalarType({
  name: 'Link',
  description: 'An Link Scalar',
  serialize: value => linkChecker(value)
})

const Date = new GraphQLScalarType({
  name: 'Date',
  description: 'Date custom scalar type',
  serialize(value) {
    return value.getTime()
  },
  parseValue(value) {
    return new Date(value)
  },
  parseLiteral(ast) {
    // eslint-disable-next-line no-undef
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 10))
    }
    return null
  }
})

module.exports = {
  Date,
  Link,
  Image,
  Query: {
    // **************************user details***********************************
    async user(_, { ID }) {
      const data = await User.findOne({ _id: ID })
      if (!data) { throw new UserInputError('User Not Found') }
      return data
    },

    async getUser(_, { limit }) {
      const data = await User.find().sort({ createdAt: -1 }).limit(limit)
      if (!data.length) { throw new UserInputError('Users Not Found') }
      return data
    },
    // ***************************Question Details*******************************
    async question(_, { ID }) {
      const data = await Questions.findOne({ _id: ID })
      if (!data) { throw new UserInputError('Question Not Found') }
      return data
    },

    async getAllQuestion(_, { limit }) {
      const data = await Questions.find().sort({ createdAt: -1 }).limit(limit)
      if (!data.length) { throw new UserInputError('Questions Not Found') }
      return data
    },
    // ***************************Answer details**********************************
    async answer(_, { ID }) {
      const data = await Answer.findOne({ _id: ID })
      if (!data) { throw new UserInputError('Answer not Found') }
      return data
    },

    async getAllAnswer(_, { limit }) {
      const data = await Answer.find().sort({ createdAt: -1 }).limit(limit)
      if (!data.length) { throw new UserInputError('Answers not Found') }
      return data
    },

    async findQuestion(_, { questionInput: { question } }) {
      const q = []
      if (question) {
        question = {
          $or: [
            { question: { $regex: new RegExp('^.*' + question + '.*', 'i') } }]
        }
        q.push(question)
        return await Questions.find({ $and: q }).lean()
      }
      return await Questions.find().lean()
    },
    // likeWiseQuestion
    async likeWiseQuestion(_, { limit }) {
      const answer = await Answer.find()
      const sortAnswer = answer.sort((a, b) => {
        return b.likes.length - a.likes.length
      })
      return sortAnswer
    }
  },

  Mutation: {
    // ***********************************user*************************************
    async loginUser(_, { loginInput: { email, password } }) {
      if (!validateEmail(email)) throw new UserInputError('Invalid Email')
      const user = await User.findOne({ email, password }).lean()
      if (!user) throw new UserInputError('Invalid Credentials try again')
      const newToken = jwt.sign({ _id: user._id }, 'linkerForYou', { expiresIn: '365d' })
      if (user.sToken === '') {
        user.sToken = newToken
      } else {
        user.sToken = ''
        user.sToken = newToken
      }
      const data = await User.findOneAndUpdate({ _id: user._id }, { sToken: user.sToken }, { new: true })
      console.log(data)
      return data
    },

    async createUser(_, { userInput: { name, email, mobile, password } }) {
      if (!validateEmail(email)) throw new UserInputError('Invalid Email')
      if (!validateMobile(mobile)) throw new UserInputError('Invalid Mobile Number')
      if (!validatePassword(password)) throw new UserInputError('Password must be 8 Digit and Use at Least one special character and one number')
      const data = await User.find({ $or: [{ email }, { mobile }] })
      if (data.length) throw new UserInputError('User Credentials already Exist')
      const createUser = new User({
        name,
        email,
        mobile,
        password
      })
      await createUser.save()
      return 'User Create Successfully'
    },

    async editUser(_, { userInput: { name, email, mobile, password } }, context) {
      if (!validateEmail(email)) throw new UserInputError('Invalid Email')
      if (!validateMobile(mobile)) throw new UserInputError('Invalid Mobile Number')
      if (!validatePassword(password)) throw new UserInputError('Password must be 8 Digit and Use at Least one special character and one number')
      const verify = await User.find({ _id: context })
      if (verify) {
        const user = await User.find({ $or: [{ email }, { mobile }], _id: { $ne: context } })
        if (user.length) throw new UserInputError('Credentials already exist')
        const updateUser = {
          name,
          email,
          mobile,
          password
        }
        console.log(context)
        const data = await User.findOneAndUpdate({ _id: context }, { updateUser }, { new: true })
        console.log(data)
        if (data.length) {
          return 'User Update Successfully'
        } else if (data) throw new UserInputError('Something went Wrong')
      }
      throw new UserInputError('Invalid User')
    },

    async logOutUser(_, { ID }, context) {
      const verify = await User.find({ _id: context })
      if (verify) {
        const data = await User.findOneAndUpdate({ _id: context }, { sToken: '' }, { new: true })
        if (data) {
          return true
        } else return false
      }
      throw new UserInputError('User Not Found')
    },

    async deleteUser(_, { ID }) {
      const data = await User.findByIdAndDelete(ID)
      if (data) {
        return true
      } else return false
    },

    // **********************************questions**********************************

    async createQuestion(_, { questionInput: { question } }, context) {
      const verify = await User.findById({ _id: context._id }).lean()
      if (verify) {
        const already = await Questions.findOne({ question })
        if (already) {
          return new ApolloError('Question Already Exists')
        }
        const createQuestion = new Questions({
          question,
          AskedBy: verify
        })
        const res = await createQuestion.save()
        await User.findOneAndUpdate({ _id: context }, { questions: res }, { new: true })
        return res
      } else return new ApolloError('Please login')
    },

    async deleteQuestion(_, { ID }, context) {
      const verify = await User.findById({ _id: context._id })
      if (verify) {
        const question = await Questions.findById(ID)
        if (!question) return new ApolloError('Question Not Found')
        if (question.AskedBy._id.equals(verify._id)) {
          await Questions.findByIdAndDelete(ID)
          await User.findOneAndUpdate({ _id: context }, { questions: {} }, { new: true })
          return 'Question Delete Successfully'
        } else return new ApolloError('You have no Permission to Delete this question')
      } else return new ApolloError('Please login')
    },

    async editQuestion(_, { ID, questionInput: { question } }, context) {
      const verify = await User.findById({ _id: context._id })
      if (verify) {
        const oldQuestion = await Questions.findById(ID)
        if (oldQuestion.AskedBy._id.equals(verify._id)) {
          const already = await Questions.findOne({ question, _id: { $ne: ID } })
          if (already) {
            return new ApolloError('Question Already Exists')
          }
          const data = await Questions.findByIdAndUpdate(ID, { question }, { new: true })
          const arr2 = [...verify.questions]
          for (let obj of arr2) {
            if (obj._id.equals(ID)) {
              obj = data
              break
            }
          }
          await User.findByIdAndUpdate({ _id: verify._id }, { questions: arr2 }, { new: true })
          if (data) {
            return data
          }
        } else return new ApolloError('You have no Permission to Change this question')
      } else return 'You have no permission'
    },

    // ***********************************Answer*************************************
    async createAnswer(_, { answerInput: { questionId, answer, description } }, context) {
      if (isValidObjectId(questionId)) {
        const qCheck = await Questions.findById({ _id: questionId })
        if (!qCheck) {
          return new ApolloError('Question Not Found')
        }
        const verify = await User.findById({ _id: context._id })
        if (verify) {
          if (questionId === '' || answer === '' || description === '') {
            return new ApolloError('please fill all details')
          }
          const createAnswer = new Answer({
            question: questionId,
            answer,
            description,
            AnswerBy: verify
          })
          const res = await createAnswer.save()
          await Questions.findByIdAndUpdate({ _id: questionId }, { answer: res }, { new: true })
          return res
        } else return new ApolloError('You have to Create Your Account and Login')
      } else return new ApolloError('Invalid Question Id')
    },

    async editAnswer(_, { ID, answerInput: { questionId, answer, description } }, context) {
      if (isValidObjectId(questionId)) {
        const qCheck = await Questions.findById({ _id: questionId })
        if (!qCheck) {
          return new ApolloError('Question Not Found')
        }
        const verify = await User.findById({ _id: context._id })
        if (verify) {
          const oldAnswer = await Answer.findById(ID).lean()
          if (oldAnswer.AnswerBy._id.equals(verify._id)) {
            const updateAnswer = {
              answer,
              description
            }
            const res = await Answer.findByIdAndUpdate(ID, updateAnswer, { new: true })
            const arr = [...qCheck.answer]
            for (const obj of arr) {
              if (obj._id.equals(ID)) {
                obj.answer = answer
                obj.description = description
                break
              }
            }
            const question = await Questions.findOneAndUpdate({ _id: questionId }, { answer: arr }, { new: true })
            const arr2 = [...verify.questions]
            for (let obj of arr2) {
              if (obj._id.equals(questionId)) {
                obj = question
                break
              }
            }
            await User.findByIdAndUpdate({ _id: verify._id }, { questions: arr2 }, { new: true })
            return res
          } else return new ApolloError('You have no Permission to Change this Answer')
        } else return new ApolloError('You have to Create Your Account and Login')
      } else return new ApolloError('Invalid Question Id')
    },

    async deleteAnswer(_, { ID }, context) {
      const verify = await User.findById({ _id: context._id })
      if (verify) {
        const answer = await Answer.findById(ID)
        if (!answer) return new ApolloError('Question Not Found')
        if (answer.AnswerBy._id.equals(verify._id)) {
          await Answer.findByIdAndDelete(ID)
          await Questions.findByIdAndUpdate({ _id: verify._id }, { $pull: { answer: { _id: ID } } })
          return 'Question Delete Successfully'
        } else return new ApolloError('You have no Permission to Delete this question')
      } else return new ApolloError('Please login')
    },

    async likeAnswer(_, { ID }, context) {
      await Answer.findByIdAndUpdate(ID, { $addToSet: { likes: context._id } })
      return true
    }
  }
}
