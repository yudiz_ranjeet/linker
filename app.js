const { ApolloServer } = require('apollo-server')
const { makeExecutableSchema } = require('@graphql-tools/schema')
const { constraintDirective, constraintDirectiveTypeDefs } = require('graphql-constraint-directive')
const { default: mongoose } = require('mongoose')
const jwt = require('jsonwebtoken')
require('dotenv').config()
const mongo = process.env.MONGO
const typeDefs = require('./apollo/typedef')
const resolvers = require('./apollo/resolver')
const cors = require('cors')
const helmet = require('helmet')
const corsOpts = {
  origin: '*',

  methods: [
    'GET',
    'POST',
    'PUT',
    'DELETE'
  ],

  allowedHeaders: [
    'Access-Control-Allow-Headers', 'Content-Type, Authorization'
  ]
}

module.exports = (app) => {
  app.use(cors(corsOpts))
  app.use(helmet())
}

async function getClaims(req) {
  let token
  try {
    token = jwt.verify(req.headers.authorization, 'linkerForYou')
    return token
  } catch (e) {
    return null
  }
}

let schema = makeExecutableSchema({
  typeDefs: [constraintDirectiveTypeDefs, typeDefs],
  resolvers
})
schema = constraintDirective()(schema)
const server = new ApolloServer({
  schema,
  context: async({ req }) => {
    const token = getClaims(req)
    return token
  },
  introspection: 'development'
})
const PORT = process.env.PORT || 5555
mongoose.connect(mongo, { useNewUrlParser: true })
  .then(() => {
    console.log('Database connected')
    return server.listen(PORT)
  })
  .then((res) => {
    console.log('server listen on Port')
  })
