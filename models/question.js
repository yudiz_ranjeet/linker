const { model, Schema } = require('mongoose')

const Question = new Schema({
  question: { type: String, trim: true },
  answer: [{ type: Object }],
  AskedBy: { type: Object },
  createdAt: { type: Date, default: Date.now() }
})

module.exports = model('questions', Question)
