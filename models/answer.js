const { model, Schema } = require('mongoose')

const Answer = new Schema({
  answer: { type: String },
  question: { type: Schema.Types.ObjectId, ref: 'questions' },
  AnswerBy: { type: Object },
  description: { type: String },
  createdAt: { type: Date, default: Date.now() },
  likes: [{ type: Schema.Types.ObjectId, ref: 'Users' }]
})

module.exports = model('answers', Answer)
