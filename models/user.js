const { model, Schema } = require('mongoose')

const User = new Schema({
  name: { type: String, trim: true },
  email: { type: String, trim: true },
  mobile: { type: String, trim: true },
  password: { type: String },
  sToken: { type: String },
  ProfilePic: { type: String, trim: true, default: 'Default/magenta_explorer-wallpaper-3840x2160.jpg' },
  createdAt: { type: Date, default: Date.now() },
  questions: [{ type: Object }]
})

module.exports = model('Users', User)
