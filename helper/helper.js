const ObjectId = require('mongoose').Types.ObjectId
const validatePassword = (pass) => {
  const regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/
  return !!(pass.match(regex))
}
function validateEmail(email) {
  const sRegexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
  return !!(email.match(sRegexEmail))
}
function validateMobile(mobile) {
  const regex = /^\d{10}$/
  return mobile.match(regex)
}
function isValidObjectId(id) {
  if (ObjectId.isValid(id)) {
    if ((String)(new ObjectId(id)) === id) { return true }
    return false
  }
  return false
}
module.exports = {
  validateEmail,
  validateMobile,
  validatePassword,
  isValidObjectId
}
